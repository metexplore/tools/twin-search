# Twin Search
A preprocessing utility for CHaMP app

## Description
TwinSearch is an RShiny tool used to detect pairs of heavy & native forms of compounds in mz data for twin-peaks analysis. It can provides a filtered list of masses that can be upload in the CHaMP app for reconstructing biotransformation pathways.

## Usage
```
R -e "shiny::runApp('app.R')"
```

## Contributing
Pull requests are welcome **on the gitlab repo** ([forgemia.inra.fr/metexplore/tools/twin-search](https://forgemia.inra.fr/metexplore/tools/twin-search)). For major changes, please open an issue first to discuss what you would like to change.  


## Authors and acknowledgment
Jean-François Martin, Clément Frainay and Marine Valleix  
INRAE TOXALIM  
Research Centre in Food Toxicology  

## License
TwinSearch is distributed under the open license [CeCILL-2.1](https://cecill.info/licences/Licence_CeCILL_V2.1-en.html) (compatible GNU-GPL).  
